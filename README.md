# C# WinForm 3D图表控件

## 简介

本项目提供了一个基于C#开发的3D图表控件，适用于WinForm项目。该控件使用了OpenTK库来绘制3D图形，具有以下特点：

- **原创代码**：所有代码均为原创，确保了代码的可靠性和可维护性。
- **颜色自定义**：图表颜色、文字颜色均可自由替换，满足不同项目的需求。
- **鼠标交互**：支持鼠标拖拽旋转图表，提供更直观的交互体验。

## 功能特性

- **3D图形绘制**：使用OpenTK库高效绘制3D图形。
- **颜色自定义**：图表颜色、文字颜色均可根据需求进行替换。
- **鼠标拖拽旋转**：支持鼠标拖拽旋转图表，方便用户从不同角度查看数据。

## 使用方法

1. **下载资源文件**：从本仓库下载资源文件。
2. **集成到项目**：将资源文件集成到你的WinForm项目中。
3. **自定义颜色**：根据需求修改图表和文字的颜色。
4. **运行项目**：运行项目，体验3D图表的交互效果。

## 依赖项

- **OpenTK**：用于3D图形绘制。
- **WinForm**：适用于WinForm项目。

## 贡献

欢迎大家贡献代码，提出改进建议。如果你有任何问题或建议，请在Issues中提出。

## 许可证

本项目采用MIT许可证，详情请参阅[LICENSE](LICENSE)文件。

---

希望这个3D图表控件能为你的项目带来便利和创新！